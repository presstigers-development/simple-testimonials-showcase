<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'presstig_plugin_sts');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':Q>Tnv;Zb|o2~8Z(KR>jy6LYjd`K>F+@`VY`+t^,])tz:ou@U)2)^rUJ1`x9_S`r');
define('SECURE_AUTH_KEY',  '_]Fx0,/k%F#1A5zITO6SD,QaH3$V20h&^b&d~&Rsd86D%XHRR/cM.2JB.D jTKW`');
define('LOGGED_IN_KEY',    '!doA})avt<Xh7d6=am/-xAz=.pFbqpl/rhf@gy]++oow~Flbz37&sbb83.bQ5i?-');
define('NONCE_KEY',        'VLO^9Q;{;o`L UKhV_7[J+tz.Y/FDz#Z)};)fEfkdbwpNw`#1U@:+5TP,@!C4~B.');
define('AUTH_SALT',        'xH`uu(^2]7>vK}t1_|}+:vngfw+MAu(.6)l+!ppLBrw#+e;TV/)1.1,9$m[g0QDD');
define('SECURE_AUTH_SALT', '|P/p-d;A`v:3aNczZqtV-:_ 9x5=Dy%.g& p >uOP@y1XP6(}O9k{Zk%XI0`FZ1P');
define('LOGGED_IN_SALT',   '2zn?Upgl(-%63]gqj)x8&U?Cz@A7OG5&yMGc$E[zBcD5T 8PUw?n%gY;%|}a*3P:');
define('NONCE_SALT',       '//xFXoPznbFg=2uJBpIdDdRa9~a}~DR1{TJMTEFG0$V1KZINYVh!.@/+7StQRg[y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sts_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
